# ************************************************************
# Sequel Pro SQL dump
# Versão 4541
#
# http://www.sequelpro.com/
# https://github.com/sequelpro/sequelpro
#
# Host: 127.0.0.1 (MySQL 5.5.5-10.1.25-MariaDB-1~trusty)
# Base de Dados: productivity_rating
# Tempo de Geração: 2019-08-01 18:59:36 +0000
# ************************************************************


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;


# Dump da tabela departments
# ------------------------------------------------------------

DROP TABLE IF EXISTS `departments`;

CREATE TABLE `departments` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `name` varchar(50) DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `departments` WRITE;
/*!40000 ALTER TABLE `departments` DISABLE KEYS */;

INSERT INTO `departments` (`id`, `name`, `updated_at`, `created_at`)
VALUES
	(1,'Tecnologia da Informação','2019-07-31 21:01:40','2019-07-30 23:22:34'),
	(11,'Marketing','2019-07-31 03:16:38','2019-07-31 02:30:18'),
	(32,'Recursos Humanos','2019-07-31 04:17:02','2019-07-31 04:17:02'),
	(33,'Vendas','2019-07-31 04:17:10','2019-07-31 04:17:10'),
	(34,'Sucesso de Cliente','2019-07-31 04:17:20','2019-07-31 04:17:20'),
	(46,'C-Level','2019-07-31 21:03:43','2019-07-31 21:03:11');

/*!40000 ALTER TABLE `departments` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela employees
# ------------------------------------------------------------

DROP TABLE IF EXISTS `employees`;

CREATE TABLE `employees` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `first_name` varchar(50) NOT NULL DEFAULT '',
  `last_name` varchar(50) NOT NULL DEFAULT '',
  `gender` varchar(1) NOT NULL DEFAULT '',
  `birth_date` date NOT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  `department_id` int(11) unsigned DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `department_id` (`department_id`),
  CONSTRAINT `employees_ibfk_1` FOREIGN KEY (`department_id`) REFERENCES `departments` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `employees` WRITE;
/*!40000 ALTER TABLE `employees` DISABLE KEYS */;

INSERT INTO `employees` (`id`, `first_name`, `last_name`, `gender`, `birth_date`, `updated_at`, `created_at`, `department_id`)
VALUES
	(5,'Alecsander','Abreu','M','1997-01-16','2019-07-31 22:47:20','2019-07-31 22:47:20',32),
	(6,'Douglas','Costa','M','1950-09-11','2019-07-31 23:21:33','2019-07-31 22:58:04',46),
	(7,'Elaine','Silva','F','1989-06-23','2019-07-31 22:58:47','2019-07-31 22:58:47',34),
	(10,'Duda','Cabral','M','1999-08-01','2019-08-01 02:34:55','2019-08-01 02:34:55',11);

/*!40000 ALTER TABLE `employees` ENABLE KEYS */;
UNLOCK TABLES;


# Dump da tabela ratings
# ------------------------------------------------------------

DROP TABLE IF EXISTS `ratings`;

CREATE TABLE `ratings` (
  `id` int(11) unsigned NOT NULL AUTO_INCREMENT,
  `value` varchar(5) DEFAULT NULL,
  `employee_id` int(11) unsigned DEFAULT NULL,
  `updated_at` timestamp NULL DEFAULT NULL,
  `created_at` timestamp NULL DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `employee_id` (`employee_id`),
  CONSTRAINT `ratings_ibfk_1` FOREIGN KEY (`employee_id`) REFERENCES `employees` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

LOCK TABLES `ratings` WRITE;
/*!40000 ALTER TABLE `ratings` DISABLE KEYS */;

INSERT INTO `ratings` (`id`, `value`, `employee_id`, `updated_at`, `created_at`)
VALUES
	(12,'down',5,'2019-08-01 02:34:13','2019-07-31 02:34:13'),
	(13,'up',10,'2019-08-01 02:35:01','2019-08-01 02:35:01'),
	(14,'down',6,'2019-08-01 02:35:20','2019-08-01 02:35:20'),
	(15,'up',7,'2019-08-01 02:35:21','2019-08-01 02:35:21');

/*!40000 ALTER TABLE `ratings` ENABLE KEYS */;
UNLOCK TABLES;



/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;
/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
