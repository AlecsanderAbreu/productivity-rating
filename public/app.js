let _document = document;
$(_document).on('click', '.btn-danger',  function () {
  var btn = this;
  $.ajax({
    url: $(this).data('href'),
    type: 'delete',
    dataType: 'json',
    success: function (data) {
      $(btn).parents('tr').remove();
    }
  })
})

$(_document).on('click', '.btn-rating',  function () {
  var btn = this;
  $.ajax({
    url: $(this).data('href'),
    type: 'post',
    data: {
      'employee_id': $(this).data('employee-id'),
      'value': $(this).data('value')
    },
    dataType: 'json',
    success: function (response) {
      $(btn).parents('td').html('<p class="text-success">'+ response.message +'</p>');
    },
    error: function (response) {  
      $(btn).parents('td').html('<p class="text-danger">'+ response.responseJSON.message +'</p>');
    }
  })
})