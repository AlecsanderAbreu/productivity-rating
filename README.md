## Avaliação de produtividade

Sistema para autoavaliação individual de produtividade diária de participantes de uma equipe.

1. Rode o composer
2. Altere as configurações do seu banco de dados no arquivo .env
3. Importe o dump mysql da base de dados que está no repositório
4. Rode na pasta raís do projeto ```php -S localhost:8080 -t public public/index.php ```
