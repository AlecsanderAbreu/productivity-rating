<?php
namespace App\Controllers;
use Slim\Views\Twig as View;
use App\Models\Rating as Rating;
use App\Models\Employee as Employee;
use Carbon\Carbon;


class ReportController extends BaseController
{

   public function index($request, $response, $args)
   {
      $rating = new Rating();

      $days = $args['days'] ?? 7;

      $total = $rating::whereDate('created_at', '>', Carbon::now()->subDays($days))->count();
      $rating_down = $rating::whereDate('created_at', '>', Carbon::now()->subDays($days))->where('value', 'down')->count();
      $rating_up = $rating::whereDate('created_at', '>', Carbon::now()->subDays($days))->where('value', 'up')->count();

      return $this->container->view->render($response, 'report.twig', [
         'title' => 'Relatórios',
         'days' => $days,
         'rating' => $total,
         'rating_down' => $rating_down,
         'rating_up' => $rating_up
      ]);
   }

}
?>