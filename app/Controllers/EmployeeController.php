<?php
namespace App\Controllers;
use Slim\Views\Twig as View;
use App\Models\Employee as Employee;
use App\Models\Department as Department;


class EmployeeController extends BaseController
{

   public function index($request, $response, $args)
   {
      $employees = Employee::get();

      return $this->container->view->render($response, 'employees/list.twig', [
         'title' => 'Funcionários',
         'employees' => $employees
      ]);
   }

   public function create($request, $response, $args)
   {

      $departments = Department::get();

      return $this->container->view->render($response, 'employees/form.twig', [
         'title' => 'Novo funcionário',
         'action' => '/funcionarios',
         'departments' => $departments,
      ]);
   }
   
   public function store($request, $response, $args)
   {

      $employee = new Employee;
      $department = Department::find($request->getParam('department_id'));

      $employee->first_name = $request->getParam('first_name');
      $employee->last_name = $request->getParam('last_name');
      $employee->gender = $request->getParam('gender');
      $employee->birth_date = date("Y-m-d", strtotime($request->getParam('birth_date')));
      $employee->department_id = $department->id;

      $employee->save();

      return $response->withRedirect('/funcionarios');
   }
   
   public function edit($request, $response, $args)
   {
      $employee = Employee::find($args['id']);
      $departments = Department::get();

      return $this->container->view->render($response, 'employees/form.twig', [
         'title' => 'Novo funcionário',
         'action' => '/funcionarios/' . $employee->id . '/editar',
         'employee' => $employee,
         'departments' => $departments
      ]);
   }

   public function update($request, $response, $args)
   {
      $employee = Employee::find($args['id']);
      $department = Department::find($request->getParam('department_id'));

      $employee->first_name = $request->getParam('first_name');
      $employee->last_name = $request->getParam('last_name');
      $employee->gender = $request->getParam('gender');
      $employee->birth_date = date("Y-m-d", strtotime($request->getParam('birth_date')));
      $employee->department_id = $department->id;

      $employee->save();

      return $response->withRedirect('/funcionarios');
   }
}
?>