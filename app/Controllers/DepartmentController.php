<?php
namespace App\Controllers;
use Slim\Views\Twig as View;
use App\Models\Department as Department;


class DepartmentController extends BaseController
{

   public function index($request, $response, $args)
   {
      $departments = Department::get();

      return $this->container->view->render($response, 'departments/list.twig', [
         'title' => 'Departamentos',
         'departments' => $departments
      ]);
   }

   public function create($request, $response, $args)
   {
      return $this->container->view->render($response, 'departments/form.twig', [
         'title' => 'Novo departamento',
         'action' => '/departamentos'
      ]);
   }
   
   public function store($request, $response, $args)
   {
      $departments = Department::create([
         'name' => $request->getParam('name')
      ]);

      return $response->withRedirect('/departamentos');
   }
   
   public function edit($request, $response, $args)
   {
      $department = Department::find($args['id']);

      return $this->container->view->render($response, 'departments/form.twig', [
         'title' => 'Novo departamento',
         'action' => '/departamentos/' . $department->id . '/editar',
         'department' => $department
      ]);
   }

   public function update($request, $response, $args)
   {
      $user = Department::find($args['id']);
      $user->name = $request->getParam('name');
      $user->save();

      return $response->withRedirect('/departamentos');
   }
}
?>