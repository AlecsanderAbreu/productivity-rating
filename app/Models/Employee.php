<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

Class Employee extends Model {

  protected $fillable = [
    'first_name',
    'last_name',
    'gender',
    'birth_date',
    'department'
  ];

  public function department()
  {
    return $this->belongsTo('App\Models\Department');
  }

  public function rating()
  {
      return $this->hasMany('App\Models\Rating');
  }

}