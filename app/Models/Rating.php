<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

Class Rating extends Model {

  protected $fillable = [
    'value',
  ];

}