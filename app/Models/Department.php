<?php
namespace App\Models;

use Illuminate\Database\Eloquent\Model;

Class Department extends Model {

  protected $fillable = ['name'];

}