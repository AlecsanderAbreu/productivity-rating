<?php
namespace App\Api;
use App\Models\Rating as Rating;
use App\Models\Employee as Employee;


class RatingApi extends BaseApi
{
  public function store($request, $response, $args)
   {
      $employee = Employee::find($request->getParam('employee_id'));

      if(isset($employee->rating->last()->created_at)){
        $times = $employee->rating->last()->created_at->format('Y-m-d');

        if($times == date('Y-m-d')){
          return $response->withStatus(400)->withJson([
            'status' => 'error',
            'message' => 'Você já enviou sua avaliação hoje.'
          ]);  
        }
      } 

      $rating = new Rating();
      $rating->value = $request->getParam('value');
      $rating->employee_id = $employee->id;
      $rating->save();

      return $response->withStatus(200)->withJson([
        'status' => 'success',
        'message' => 'Avaliação enviada!'
      ]);
   }
}