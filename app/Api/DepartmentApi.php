<?php
namespace App\Api;
use App\Models\Department as Department;


class DepartmentApi extends BaseApi
{
  public function destroy($request, $response, $args)
   {
      $department = Department::find($args['id']);
      $department->delete();

      return $response->withStatus(200)->withJson([
        'status' => 'success'
      ]);
   }
}