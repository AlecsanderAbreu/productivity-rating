<?php
namespace App\Api;
use App\Models\Employee as Employee;


class EmployeeApi extends BaseApi
{
  public function destroy($request, $response, $args)
   {
      $employee = Employee::find($args['id']);
      $employee->delete();

      return $response->withStatus(200)->withJson([
        'status' => 'success'
      ]);
   }
}