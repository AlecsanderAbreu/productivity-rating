<?php

use Slim\App;
use Slim\Http\Request;
use Slim\Http\Response;

return function (App $app) {
    $container = $app->getContainer();

    $app->group('/departamentos', function (App $app) {
        $app->get('', 'DepartmentController:index');
        $app->get('/novo', 'DepartmentController:create');
        $app->get('/{id}/editar', 'DepartmentController:edit');
        $app->post('', 'DepartmentController:store');
        $app->post('/{id}/editar', 'DepartmentController:update');
    });
    
    $app->group('/funcionarios', function (App $app) {
        $app->get('', 'EmployeeController:index');
        $app->get('/novo', 'EmployeeController:create');
        $app->get('/{id}/editar', 'EmployeeController:edit');
        $app->post('', 'EmployeeController:store');
        $app->post('/{id}/editar', 'EmployeeController:update');
    });
    
    $app->group('/relatorios', function (App $app) {
        $app->get('/[{days}]', 'ReportController:index');
        $app->get('', 'ReportController:index');
    });

    $app->group('/api', function (App $app) {
        $app->delete('/departamentos/{id}', 'DepartmentApi:destroy');
        $app->delete('/funcionarios/{id}', 'EmployeeApi:destroy');

        $app->group('/avaliacoes', function (App $app) {
            $app->post('', 'RatingApi:store');
        });
    });

    $app->group('/', function (App $app) {
        $app->get('', 'HomeController:index');
    });
};
