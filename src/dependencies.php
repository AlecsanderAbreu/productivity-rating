<?php

use Slim\App;

return function (App $app) {
    $container = $app->getContainer();

    // view renderer
    $container['renderer'] = function ($c) {
        $settings = $c->get('settings')['renderer'];
        return new \Slim\Views\PhpRenderer($settings['template_path']);
    };

    // monolog
    $container['logger'] = function ($c) {
        $settings = $c->get('settings')['logger'];
        $logger = new \Monolog\Logger($settings['name']);
        $logger->pushProcessor(new \Monolog\Processor\UidProcessor());
        $logger->pushHandler(new \Monolog\Handler\StreamHandler($settings['path'], $settings['level']));
        return $logger;
    };

    // twig
    $container['view'] = function ($container) {
        $settings = $container->get('settings')['renderer'];
        $view = new \Slim\Views\Twig($settings['template_path'], [
            'cache' => false
        ]);
    
        // Instantiate and add Slim specific extension
        $router = $container->get('router');
        $uri = \Slim\Http\Uri::createFromEnvironment(new \Slim\Http\Environment($_SERVER));
        $view->addExtension(new \Slim\Views\TwigExtension($router, $uri));
    
        return $view;
    };

    // Eloquent ORM
    $capsule = new \Illuminate\Database\Capsule\Manager;
    $capsule->addConnection($container['settings']['db']);
    $capsule->setAsGlobal();
    $capsule->bootEloquent();
    $container['db'] = function ($container) {
        return $capsule;
    };

    // Controllers
    $container['DepartmentController'] = function ($container) {
        return new \App\Controllers\DepartmentController($container);
    };
    
    $container['EmployeeController'] = function ($container) {
        return new \App\Controllers\EmployeeController($container);
    };
    
    $container['HomeController'] = function ($container) {
        return new \App\Controllers\HomeController($container);
    };
    
    $container['ReportController'] = function ($container) {
        return new \App\Controllers\ReportController($container);
    };

    // API
    $container['DepartmentApi'] = function ($container) {
        return new \App\Api\DepartmentApi($container);
    };
    
    $container['EmployeeApi'] = function ($container) {
        return new \App\Api\EmployeeApi($container);
    };
    
    $container['RatingApi'] = function ($container) {
        return new \App\Api\RatingApi($container);
    };
};
